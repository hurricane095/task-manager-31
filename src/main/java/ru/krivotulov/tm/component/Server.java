package ru.krivotulov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.endpoint.Operation;
import ru.krivotulov.tm.api.service.IPropertyService;
import ru.krivotulov.tm.dto.request.AbstractRequest;
import ru.krivotulov.tm.dto.response.AbstractResponse;
import ru.krivotulov.tm.task.AbstractServerTask;
import ru.krivotulov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Server
 *
 * @author Aleksey_Krivotulov
 */
public final class Server {

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

    public void submit(@NotNull final AbstractServerTask abstractServerTask) {
        executorService.submit(abstractServerTask);
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(Class<RQ> regClass, Operation<RQ, RS> operation) {
        dispatcher.registry(regClass, operation);
    }

    public Object call(final AbstractRequest request) {
        return dispatcher.call(request);
    }

}
