package ru.krivotulov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.krivotulov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * FileScanner
 *
 * @author Aleksey_Krivotulov
 */
public final class FileScanner {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File("./");

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(command -> this.commands.add(command.getName()));
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process(){
        for (File file : folder.listFiles()){
            if(file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final  boolean check = this.commands.contains(fileName);
            if(check){
                bootstrap.runCommand(fileName);
                file.delete();
            }
        }
    }

    public void start() {
        init();
    }

}
