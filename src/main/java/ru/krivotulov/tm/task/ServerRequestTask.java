package ru.krivotulov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.component.Server;
import ru.krivotulov.tm.dto.request.AbstractRequest;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * ServerRequestTask
 *
 * @author Aleksey_Krivotulov
 */
public final class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    @Override
    @SneakyThrows
    public void run() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        @NotNull final AbstractRequest request = (AbstractRequest) object;
        @Nullable Object response = server.call(request);
        @NotNull final OutputStream outputStream = socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket));
    }

}
