package ru.krivotulov.tm.api.endpoint;

import ru.krivotulov.tm.dto.request.AbstractRequest;
import ru.krivotulov.tm.dto.response.AbstractResponse;

/**
 * Operation
 *
 * @author Aleksey_Krivotulov
 */
@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
