package ru.krivotulov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * ServerVersionResponse
 *
 * @author Aleksey_Krivotulov
 */
@Getter
@Setter
@NoArgsConstructor
public class ServerVersionResponse extends AbstractResponse {

    private String version;

}
