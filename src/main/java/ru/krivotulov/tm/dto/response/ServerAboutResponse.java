package ru.krivotulov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

/**
 * ServerAboutResponse
 *
 * @author Aleksey_Krivotulov
 */
@Getter
@Setter
@NoArgsConstructor
public class ServerAboutResponse extends AbstractResponse {

    @Nullable
    private String email;

    @Nullable
    private String name;

}
