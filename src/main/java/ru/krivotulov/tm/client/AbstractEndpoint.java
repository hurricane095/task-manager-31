package ru.krivotulov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.Socket;

/**
 * AbstractEndpoint
 *
 * @author Aleksey_Krivotulov
 */
@Setter
@Getter
public abstract class AbstractEndpoint {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    private Socket socket;

    protected Object call(final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    private OutputStream getOutputStream() throws IOException {
        return socket.getOutputStream();
    }

    private InputStream getInputStream() throws IOException {
        return socket.getInputStream();
    }

    public void connect() throws IOException {
        socket = new Socket(host, port);
    }

    public void disconnect() throws IOException {
        socket.close();
    }

}
