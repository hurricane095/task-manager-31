package ru.krivotulov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.model.ICommand;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.util.ListUtil;

import java.util.Collection;
import java.util.Optional;

/**
 * HelpCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class HelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String DESCRIPTION = "Display list of terminal commands.";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        Optional.ofNullable(ListUtil.emptyListToNull(commands))
                .ifPresent(list -> list.forEach(System.out::println));
    }

}
