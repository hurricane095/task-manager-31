package ru.krivotulov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Optional;

/**
 * UserRemoveCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-remove";

    @NotNull
    public static final String DESCRIPTION = "User remove.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMINISTRATOR};
    }

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.readLine();
        getUserService().deleteByLogin(login);
    }

}

