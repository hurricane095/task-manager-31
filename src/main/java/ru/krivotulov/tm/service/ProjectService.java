package ru.krivotulov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.exception.entity.ProjectNotFoundException;
import ru.krivotulov.tm.exception.field.*;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.repository.AbstractRepository;
import ru.krivotulov.tm.repository.ProjectRepository;

import java.util.*;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId,
                          @Nullable final String name
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description,
                          @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return Optional.of(create(userId, name, description))
                .map(project -> {
                    project.setDateBegin(dateBegin);
                    project.setDateEnd(dateEnd);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);

    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(project -> {
                    project.setName(name);
                    project.setDescription(description);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(project -> {
                    project.setName(name);
                    project.setDescription(description);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project changeStatusById(@Nullable final String userId,
                                    @Nullable final String id,
                                    @Nullable final Status status
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if(status == null || !Arrays.asList(Status.values()).contains(status)) throw new StatusIncorrectException();
        return Optional.ofNullable(findOneById(userId, id))
                .map(project -> {
                    project.setStatus(status);
                    project.setUserId(userId);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public Project changeStatusByIndex(@Nullable final String userId,
                                       @Nullable final Integer index,
                                       @Nullable final Status status
    ) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        if(status == null || !Arrays.asList(Status.values()).contains(status)) throw new StatusIncorrectException();
        return Optional.ofNullable(findOneByIndex(userId, index))
                .map(project -> {
                    project.setStatus(status);
                    project.setUserId(userId);
                    return project;
                }).orElseThrow(ProjectNotFoundException::new);
    }

}
